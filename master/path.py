#!/usr/bin/python
#-*- encoding: utf-8 -*-
import vector as vec
import sector as sec
import matplotlib.pyplot as plt
from matplotlib.path import Path as mPath
import matplotlib.patches as mpatches
import copy
import math
import config
import time
 

class Path(object):
    def __init__(self, oAB, conflictSecs, r = config.r):
        # center of circle 
        self.oAB = oAB
        # radius of circle
        self.r = r
        # list of conflict sectors in path, initialized with fingers - they are clockwise(!)
        self.conflictSecs = conflictSecs


if __name__ == "__main__":
    oAB = vec.Point(1.0, 0.0)
    pSec1_A = vec.Point(1.0, 1.0)
    pSec1_B = vec.Point(1.0, -1.0)
    pSec2_A = vec.Point(2.0, 0.0)
    pSec2_B = vec.Point(1.0 - 2**0.5/2, 2.0**0.5/2)
    pSec3_A = vec.Point(1.0, -1.0)
    pSec3_B = vec.Point(0.0, 0.0)
    pts = [(pSec1_A, pSec1_B), (pSec2_A, pSec2_B), (pSec3_A, pSec3_B)]
    print(pts[0])
    print(pts[-1])
    print(pts[0][0])
    print(pts[0][-1])
    print(pts[-1][0])
    print(pts[-1][-1])
    print(pts[0][0].x)
    print(pts[0][-1].x)
    print(pts[-1][0].x)
    print(pts[-1][-1].x)
