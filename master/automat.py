#!/usr/bin/python
#-*- encoding: utf-8 -*-

# path generation and partition with conflict relations
import vector as vec
import sector as sec
import path as pth
# ROS
import rospy
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
from nav_msgs.msg import Odometry
from std_msgs.msg import UInt8
# global constants
import config
# others
import copy
import math
from operator import attrgetter
from collections import namedtuple

# minimum diff angle
MIN_DIFF = 0.005

# Struct for information about point
# Simplified implementation, critical point can only refer to a single resource
SpecialPt = namedtuple('SpecialPt', ['angle', 'type', 'resource'])

defType = ['A', 'C', 'R']
 
# Class for requests - has ID of robot and assigned resources
# Has to be accepted or rejected by supervisor
class Request(object):
    def __init__(self, robotID, resources):
        self.robotID = robotID
        self.resources = resources # can store multiple resources only in merged request (i.e. [1,2])
        self.isAccepted = False

    def __str__(self): # For printing
        return ("Robot ID: " + str(self.robotID) + " Resources: " + str(self.resources) + " Accepted? " + str(self.isAccepted))

    def __repr__(self): # For printing
        return ("Robot ID: " + str(self.robotID) + " Resources: " + str(self.resources) + " Accepted? " + str(self.isAccepted))

# Class of robot
class Turtlebot(object):
    def __init__(self, id, clockwise, pStart, ncycle, path):
        # ID of robot
        self.id = id
        # clockwise of Path (True or False)
        self.clockwise = clockwise
        # starting point
        self.pStart = pStart
        # number of cycles through given sectors
        self.ncycle = ncycle
        # path.path - list of sectors, listed from 0 
        self.path = path
        # fill out angles for sectors
        self.calcAngularCoord()
        # list of special points
        self.specPts = []
        # iterator of list specPts
        self.it = 0
        # type of uNdefined pts, something went wrong
        self.type = 'N'

        # # ROS topics 
        # ATTENTION - always more than one robot!
        # Or topics in ROS and Stage will be different
        # publisher of velocities for robot
        self.velocityPub = rospy.Publisher(
            '/robot_'+str(self.id)+'/cmd_vel', Twist, queue_size=10)
        # publisher of entering to new stage for supervisor
        self.newStagePub = rospy.Publisher(
            '/new_stage_info', UInt8, queue_size = 10)
        # publisher of request for GEPP to supervisor
        self.RequestGEPP_Pub = rospy.Publisher(
            '/GEPP_request', UInt8, queue_size = 10) 
        # subscriber for odometry topic from Stage
        self.poseSub = rospy.Subscriber(
            '/robot_'+str(self.id)+'/odom', Odometry, self.updatePose)
        # rate parameter - Stage has hard-coded 10 Hz rate anyway.
        # It means problems with bad accuracy of positions and angles
        self.rate = rospy.Rate(25)

        # pose - position in vec.Point(X, Y)
        # Actualized in callback of Odometry
        self.pose = pStart
        # poseAngle - position in radians, based on pose
        self.poseAngle = 0.0
        # previous poseAngle - for checking, if the cycle ended
        self.prevAngle = 0.0   
        # current cycle
        self.currCycle = 1
        # current linear velocity
        self.currentLinVel = 0.0
        
        # flags for supervisor
        self.isAllowed = True
        self.isFinished = False
        # request list for method getRequests()
        self.requests = []
        print(
            f'''Robot {self.id} created with topics:
            velocityPub: {self.velocityPub}
            newStagePub: {self.newStagePub}
            requestPub: {self.RequestGEPP_Pub}
            poseSub: {self.poseSub}
            ''')


    # Calculate angleA, angleB for every sector
    def calcAngularCoord(self):
        if self.clockwise:
            for s in self.path.conflictSecs:
                s.angleA = sec.angleVectors(s.pA, self.pStart, self.path.oAB)
                s.angleB = sec.angleVectors(s.pB, self.pStart, self.path.oAB)
        else: # self.clockwise == False
            for s in self.path.conflictSecs:
                s.angleA = sec.angleVectors(self.pStart, s.pA, self.path.oAB)
                s.angleB = sec.angleVectors(self.pStart, s.pB, self.path.oAB)
    # Calculate special points
    # We assume that angle always grow with passed distance, independent - clockwise or not
    def calcSpecialPts(self,  alpha = config.alpha, 
                    brakeDist = config.brakingDist, rho = config.rho):
        angleAlpha = alpha/self.path.r
        angleCrit = brakeDist/self.path.r
        angleRelease = rho/self.path.r
        if self.clockwise:
            for s in self.path.conflictSecs:
                self.specPts.extend([
                    SpecialPt(max(0.01, s.angleA - angleAlpha), defType[0], s.resource),
                    SpecialPt(max(0.01, s.angleA - angleCrit), defType[1], s.resource),
                    SpecialPt(s.angleB + angleRelease, defType[2], s.resource)])
        else: # counterclockwise
            for s in self.path.conflictSecs:
                self.specPts.extend([
                    SpecialPt(max(0.01, s.angleB - angleAlpha), defType[0], s.resource),
                    SpecialPt(max(0.01, s.angleB - angleCrit), defType[1], s.resource),
                    SpecialPt(s.angleA + angleRelease, defType[2], s.resource)])
        self.specPts = copy.deepcopy(self.sortSpecPtsByAngle())
        if self.id == 0 or self.id == 1:
            print("Special pts for robot ", self.id)
            for pt in self.specPts:
                print(pt)

    # Sort points from list of named tuples in self.specPts by their angle
    def sortSpecPtsByAngle(self):
        # return sorted(self.specPts, key = attrgetter('angle'))
        return sorted(self.specPts, key = lambda pt: pt[0])

    # Callback from odometry, actualizing pose and yaw of the robot
    # if previous angle are bigger than current --> cycle++ 
    # ^--- This isn't working like we thought..?
    # Transition to new cycle moved to checkReachedPts
    def updatePose(self, msg):
        poseX = msg.pose.pose.position.x
        poseY = msg.pose.pose.position.y
        self.pose = vec.Vector(poseX, poseY)
        if self.clockwise:
            if (abs(self.poseAngle - sec.angleVectors(self.pose, self.pStart, self.path.oAB)) > MIN_DIFF*1/self.path.r):
                self.poseAngle = sec.angleVectors(self.pose, self.pStart, self.path.oAB)
        else:
            if (abs(self.poseAngle - sec.angleVectors(self.pose, self.pStart, self.path.oAB)) > MIN_DIFF*1/self.path.r):
                self.poseAngle = sec.angleVectors(self.pStart, self.pose, self.path.oAB)
        if not (self.prevAngle < MIN_DIFF):
            if self.poseAngle < self.prevAngle:
                self.currCycle += 1
                if self.currCycle > self.ncycle:
                    self.isFinished = True
                self.it = 0
                # print('Cycle: ', self.currCycle)
        if not self.isFinished:
            self.checkReachedPts()
        if self.clockwise:
            if (abs(self.poseAngle - sec.angleVectors(self.pose, self.pStart, self.path.oAB)) > 10*MIN_DIFF*1/self.path.r):
                # GEPP
                msg = UInt8()
                msg.data = 1
                self.RequestGEPP_Pub.publish(msg)
        else:
            if (abs(self.poseAngle - sec.angleVectors(self.pose, self.pStart, self.path.oAB)) > 10*MIN_DIFF*1/self.path.r):
                # GEPP
                msg2 = UInt8()
                msg2.data = 1
                self.RequestGEPP_Pub.publish(msg2)

        self.moveStep()
        self.prevAngle = self.poseAngle
            


    # Checking if special points on list specPts are reached 
    # Return index of last reached special point
    def checkReachedPts(self):
        nSpecPts = len(self.specPts)
        currAngle = self.poseAngle
        while ( (self.it != nSpecPts) and (currAngle > self.specPts[self.it][0]) and (abs(currAngle - self.specPts[self.it][0])> MIN_DIFF*1/self.path.r) ):
            # print('Point %r reached by robot %r!') % (self.it, self.id)
            # print('Type of point: ', self.specPts[self.it][1])
            # print('Angle of this point: ', self.specPts[self.it][0])
            print('Robot: ', self.id, " there, while it = ", self.it)              
            if self.specPts[self.it][1] == 'C':
                print('Approached C: ', self.id)
                print('Info: ', self.specPts[self.it])
                print()
                self.type = 'C'
                #self.actualizeIsAllowed()
                # entering new stage
                #if self.isAllowed:
                #if self.requests[0].isAccepted:
                #    del self.requests[0]
                self.isAllowed = False
                self.enterNewStage()
                # else:
                #    self.isAllowed = False
            elif self.specPts[self.it][1] == 'R':
                print('Approached R: ', self.id)
                print('Info: ', self.specPts[self.it])
                print()
                self.type = 'R'
                # entering new stage
                self.enterNewStage()
                # GEPP
                msg2 = UInt8()
                msg2.data = 1
                self.RequestGEPP_Pub.publish(msg2)
                pass
            elif self.specPts[self.it][1] == 'A':
                print('Approached A: ', self.id)
                print('Info: ', self.specPts[self.it])
                print()
                req = Request(self.id, [self.specPts[self.it][2]])
                self.requests.append(req) 
                # GEPP
                msg = UInt8()
                msg.data = 1
                self.RequestGEPP_Pub.publish(msg)
            else:
                raise ValueError('checkReachedPts - no type of special point')
            self.it += 1
            

    # Logic associated with entering new stage
    def enterNewStage(self):
        msg = UInt8()
        msg.data = self.id
        self.newStagePub.publish(msg)


    # Function for moving every Turtlebot on arcs.
    def moveStep(self):
        velMsg = Twist()
        if self.isFinished:
            velMsg.angular.z = 0.0
            velMsg.linear.x = 0.0
        elif self.isAllowed:
            # self.currentLinVel = min(1.01 * self.currentLinVel + 0.01, config.vMax)
            # velMsg.linear.x = self.currentLinVel
            self.currentLinVel = config.vMax
            velMsg.linear.x = config.vMax
            if self.clockwise:
                velMsg.angular.z = - self.currentLinVel / self.path.r
            else:
                velMsg.angular.z = self.currentLinVel / self.path.r
        else:
            # self.currentLinVel = max(round(0.65 * self.currentLinVel, 4), 0.0)
            # velMsg.linear.x = self.currentLinVel
            self.currentLinVel = 0.0
            velMsg.linear.x = 0.0
            if self.clockwise:
                velMsg.angular.z = - self.currentLinVel / self.path.r
            else: 
                velMsg.angular.z = self.currentLinVel / self.path.r
        self.velocityPub.publish(velMsg)

    # Returns remaining distance
    def getRemainingDistance(self):
        return abs((self.ncycle+0.1) * 2.0 * math.pi - (self.poseAngle + (self.currCycle - 1) * 2.0 * math.pi))

    # Returns merged request of robot (merged resources)
    def getRequests(self):
        wantedResources = []
        for req in self.requests:
            wantedResources.extend(req.resources)
        return Request(self.id, wantedResources)


# Struct for information about point
# SpecialPt = namedtuple('SpecialPt', ['angle', 'type', 'resource'])
# Struct for information about point
# defType = ['A', 'C', 'R']

    # Returns stages for this robot
    # Assuming that the special points array is already initialized
    def getStages(self):
        stages = [[]]
        for p in self.specPts:
            if p.type == 'C':
                stages.append(stages[-1] + [p.resource])
            elif p.type == 'R':
                tmpStage = copy.deepcopy(stages[-1])
                if p.resource in tmpStage:
                    tmpStage.remove(p.resource)
                stages.append(tmpStage)
        tmpStages = copy.deepcopy(stages)
        del tmpStages[0]
        for i in range(1,self.ncycle):
            stages += tmpStages
        return stages
            


if __name__ == "__main__":
    # startingPts = [ vec.Point(4.0, 13.0),\
    #                 vec.Point(8.0, 13.0),
    #                 vec.Point(12.0, 13.0), 
    #                 vec.Point(6.0, 3.0),
    #                 vec.Point(10.0, 3.0)]
    # clockwises = [True, False, True, False, True]
    try:
        rospy.init_node('turtlebot_controller', anonymous=True)
        pStart = vec.Point(1.0, 1.0)
        # Sector(pA, pB, resource)
        conflictSecs1 = [sec.Sector(vec.Point(1.6, 0.8), vec.Point(1.8,  0.6), 1),\
                        sec.Sector(vec.Point(0.0, 0.0), vec.Point(2.0,  0.0), 1),\
                        sec.Sector(vec.Point(1.6, 0.8), vec.Point(2.0,  0.0), 1),\
                        sec.Sector(vec.Point(1.6, 0.8), vec.Point(-1.0, 1.0), 1)]
        path1 = pth.Path(vec.Point(1.0, 0.0), conflictSecs1)
        tb1 = Turtlebot(1, True, pStart, 1, path1)
        tb1.calcSpecialPts()
        for p in tb1.specPts:
            print(p)
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
