#!/usr/bin/python
#-*- encoding: utf-8 -*-

# Global constans:

# amountOfRobots = 6 # number of robots
amountOfRobots = 2 # number of robots
# amountOfRobots = 4 # number of robots

rho = 0.20 # [m], radius of robot size

r = 1 # [m], radius of turning circle arcs


vMax = 0.20 # [m/s], maximum linear velocity for robots

omegaMax = 0.6 #[rad/s], maximum angular velocity for robots

aBrake = 0.6 # [m/s**2], decceleration in braking
brakingDist = vMax**2 / (2 * aBrake) + 1.2*rho #[m], braking distance from Vmax
alpha = brakingDist + 0.05 # [m]
