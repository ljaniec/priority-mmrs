# !/usr/bin/python
#-*- encoding: utf-8 -*-

import copy

class ResourceList(object):

    def __init__(self, stageList):
        self.stagesList = stageList

    # Check if the state is POrdered
    def isPOrdered(self, state, fleet):
        occupied = self.getAllocatedResources(state)
        remain = []
        for i in range(len(state)):
            remain_i = []
            for j in range(state[i], len(self.stagesList[i])):
                if not self.stagesList[i][j]:
                    break
                remain_i += self.stagesList[i][j]
            remain.append(list(set(remain_i)))

        vehicleList = list(range(len(state)))
        while True:
            lackOfProgress = True
            newList = list(copy.deepcopy(vehicleList))
            for i in vehicleList:
                intersection = [val for val in remain[i] if val in occupied]
                if (set(intersection) == set(self.stagesList[i][state[i]])) or (not remain[i]):
                    newList.remove(i)
                    for x in self.stagesList[i][state[i]]:
                        occupied.remove(x)
                    lackOfProgress = False
            vehicleList = copy.deepcopy(newList)
            if len(newList) == 0 or lackOfProgress:
                # print("lackOfProgress: ", lackOfProgress)
                break
        if len(vehicleList) == 0:
            print("State ", state, " is safe!")
            return True
        print("Stan ", state, " is not safe!")
        return False

    def getAllocatedResources(self, state):
        resources = []
        for i in range(len(self.stagesList)):
            resources += self.stagesList[i][state[i]]
        return resources
