# !/usr/bin/python
#-*- encoding: utf-8 -*-

# We assume that we have a set of solutions and fleet of robots

# Heuristics - we grant enter permission to robots with the 
# longest total remaining distance to travel - minimalization
# of time needed to complete the entire task
def criterium1(solution, fleet):
    score = 0.0
    # print('Criterium for solution:')
    # print(solution)
    for request in solution.reqStruct:
        if request.isAccepted:
            print('Robot ID: ', request.robotID)
            score += fleet[request.robotID].getRemainingDistance()
    print('Score from criteria: ', score)
    solution.value = score

# Heuristics - we prioritize solutions with the highest number of
# the allowed robots, regardless of their remaining distance -
# minimalization of the total time needed to complete tasks of all robots
def criterium2(solution, fleet):
    score = 0.0
    for request in solution.reqStruct:
        if request.isAccepted:
            score += 1.0
    solution.value = score
