#!/usr/bin/python
#-*- encoding: utf-8 -*-
import copy
import vector as vec
import math
import config

# Return positive value of angle as modulus of two pi's, [0;2*pi)
def mod2pi(theta):
    return theta - 2.0 * math.pi * math.floor(theta / 2.0 / math.pi)

# Return angle like atan2(), from [0;2*pi) to (-pi;pi]
def pi2pi(angle):
    while(angle >= math.pi):
        angle = angle - 2.0 * math.pi

    while(angle <= -math.pi):
        angle = angle + 2.0 * math.pi

    return angle

# Angle between vectors (going counterclockwise from pA to pB)
# https://stackoverflow.com/questions/21483999/\
# using-atan2-to-find-angle-between-two-vectors
def angleVectors(pA, pB, oAB):
    angle = math.atan2(pB[1]-oAB[1], pB[0]-oAB[0]) \
                        - math.atan2(pA[1]-oAB[1], pA[0]-oAB[0])
    if angle < 0:
        angle += 2 * math.pi
    return angle


class Sector(object):
    def __init__(self, pA, pB, resource):
        self.pA = pA
        self.angleA = -1.0 # negative - uninitialized
        self.pB = pB
        self.angleB = -1.0 # negative - uninitialized
        self.resource = resource

    # TODO: fix for new
    def __eq__(self, otherSector):
        return isinstance(otherSector, Sector) and \
                    self.pA == otherSector.pA and \
                    self.angleA == otherSector.angleA and \
                    self.pB == otherSector.pB and \
                    self.angleB == otherSector.angleB and \
                    self.resource == otherSector.resource

    def __repr__(self):
        return ('(Circle Arc, pA: %s, angleA: %s, pB: %s, angleB: %s, resource: %s)' % (self.pA, self.angleA, self.pB, self.angleB, self.R))

    def __hash__(self):
        return hash(self.__repr__())

    # Return:
    # sec[0] - sec.pA, 
    # sec[1] - sec.pB,
    # sec[2] - sec.R
    def __getitem__(self, key):
        if  (key == 0):
            return self.pA
        elif (key == 1):
            return self.pB
        elif (key == 2):
            return self.R
        else:
            raise ValueError("Wrong index in __getitem__")


if __name__ == "__main__":
    pass