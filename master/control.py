# !/usr/bin/python
#-*- encoding: utf-8 -*-

# path generation and partition with conflict relations
import vector as vec
import sector as sec
import path as pth
import automat as turtle
import resourceList
import solution as sol
import criteria as crit
# ROS
import rospy
from std_msgs.msg import UInt8
# global constans
import config
# others
import copy
import threading
import time
import math

# GLOBALS
lock = threading.Lock() # mutex for state vector
state = [0] * config.amountOfRobots # vector of current stages of all robots
fleet = [] # fleet of robots
resourceList # After creating fleet - you need to initialize resourceList for them

# GEPP - Granting Enter Permission Procedure for a robot
# dummy - we are only interested in triggering GEPP, the argument is irrelevant
def GEPP(dummy):
    lock.acquire()
    requestStruct = []
    try:
        global state
        global fleet
        #print('State: ', state)
        for robot in fleet:
            if len(robot.requests):
                requestStruct.append(robot.getRequests())
        if not len(requestStruct):
            return 
        #print('Allocated resource:')
        #print(resourceList.getAllocatedResources(state))
        # Generating of solutions
        solutions = sol.generateAllSolutions( requestStruct,\
                                        resourceList.getAllocatedResources(state))
        for s in solutions:
            crit.criterium1(s,fleet)
            
        sortedSol = sorted(solutions, key = lambda solution: solution.value)[::-1]
       
        while True:
            tmp_state = copy.deepcopy(state)
            for r in sortedSol[0].reqStruct:
                if r.isAccepted:
                    tmp_state[r.robotID] += 1
            if resourceList.isPOrdered(tmp_state,fleet):
                print("GEPP - state allowed: ", tmp_state)
                print("sortedSol[0].reqStruct: ", sortedSol[0].reqStruct)
                for r in sortedSol[0].reqStruct:
                    if r.isAccepted:
                        print('We accepted ', r.robotID)
                        for r2 in fleet[r.robotID].requests:
                            r2.isAccepted = True
                            #fleet[r.robotID].isAllowed = True
                        if fleet[r.robotID].isAllowed == False:
                            # Going back to travel after waiting at cp
                            fleet[r.robotID].isAllowed = True
                            del fleet[r.robotID].requests[0]
                            state[r.robotID] += 1
                    else:
                        print('We rejected ', r.robotID)
                        for r2 in fleet[r.robotID].requests:
                            r2.isAccepted = False
                break
            else:
                del sortedSol[0]

        # do testów pierwszy zawsze jedzie
        #for request in fleet[0].requests:
        #    request.isAccepted = True
    finally:
        lock.release()

# Update based on ROS newStagePublisher from robot with given ID
def updateState(id):
    ###iter = fleet[int(id.data)].it
    lock.acquire()
    try:
        global state
        id = int(id.data)
        print("UpdateState() from robot: ", id)
        print("State before: ", state)
        if fleet[id].type == 'N':
            print("Point of type = N (!)")
        type = fleet[id].type
        fleet[id].type = 'N'
        if type == 'R':
            state[id] += 1
        #fleet[id].it += 1
        elif type == 'C':
            if not fleet[id].requests:
                print('GEPP allowed, ok')
                return
            elif fleet[id].requests[0].isAccepted:
                del fleet[id].requests[0]
                fleet[id].isAllowed = True
                state[id] += 1
            else:
                print("Entry not allowed...")
        print("State after: ", state)
    finally:
        lock.release()

# # Returns list of robots sorted by their remaining distance ascend
def sortRobotsByRemainingDistance(robotFleet):
    return sorted(robotFleet, key = lambda robot: robot.getRemainingDistance())

if __name__ == "__main__":
    # startingPts = [ vec.Point(4.0, 13.0),\
    #                 vec.Point(8.0, 13.0),
    #                 vec.Point(12.0, 13.0), 
    #                 vec.Point(6.0, 3.0),
    #                 vec.Point(10.0, 3.0)]
    # clockwises = [True, False, True, False, True]
    try:
        # EXAMPLE FOR N = 2 ROBOTS
        # rospy.init_node('turtlebot_controller', anonymous=True)
        # GEPPSubscriber = rospy.Subscriber('/GEPP_request', UInt8, GEPP)
        # newStageSubscriber = rospy.Subscriber('/new_stage_info', UInt8, updateState)
        # ################################################
        # # Template for adding a robot
        # ################################################
        # pStart1 = vec.Point(1.0, 3.0)
        # clockwise1 = True
        # ncycle1 = 3
        # oAB1 = vec.Point(2.0, 3.0)
        # r1 = 1
        # conflictSecs1 = \
        #     [sec.Sector(vec.Point(1.4749690926348, 3.851083160632), 
        #                 vec.Point(2.8714309073652, 3.490518270494), 1),
        #      sec.Sector(vec.Point(2.8714309073652, 2.509481729506), 
        #                 vec.Point(1.4749690926348, 2.148916839368), 2)]
        # path1 = pth.Path(oAB1, conflictSecs1, r1)
        # tb1 = turtle.Turtlebot(0, clockwise1, pStart1, ncycle1, path1)
        # tb1.calcSpecialPts()
        # fleet.append(tb1)
        # ###############################################
        # pStart2 = vec.Point(4.0, 5.0)
        # ncycle2 = 3
        # clockwise2 = False
        # oAB2 = vec.Point(4.0, 3.0)
        # r2 = 2
        # conflictSecs2 = \
        #     [sec.Sector(vec.Point(2.0109501621872, 3.2089993844418),
        #                 vec.Point(2.7662498378128, 4.5741221481521), 1),
        #      sec.Sector(vec.Point(2.7662498378128, 1.4258778518479),
        #                 vec.Point(2.0109501621872, 2.7910006155582), 2)]
        # path2 = pth.Path(oAB2, conflictSecs2, r2)
        # tb2 = turtle.Turtlebot(1, clockwise2, pStart2, ncycle2, path2)
        # tb2.calcSpecialPts()
        # fleet.append(tb2)
        # # Show stages of robots with requested resources for creating resourceList
        # stages = []
        # for robot in fleet:
        #     stages.append(robot.getStages())
        #     # print(robot.getStages())
        # resourceList = resourceList.ResourceList(stages)
        # print('Resource List:')
        # print(resourceList.getAllocatedResources([1,2]))
        # rospy.spin()
        ###################################################################################
        # EXAMPLE FOR N = 5 ROBOTS (ARTICLE)
        ###################################################################################
        rospy.init_node('turtlebot_controller', anonymous=True)
        GEPPSubscriber = rospy.Subscriber('/GEPP_request', UInt8, GEPP)
        newStageSubscriber = rospy.Subscriber('/new_stage_info', UInt8, updateState)
        # ################################################
        # # Template for adding a robot 1-6
        # ################################################
        # pStart1 = vec.Point(2.00122, -0.20054)
        # clockwise1 = False
        # ncycle1 = 5
        # oAB1 = vec.Point(1.6, -0.2)
        # r1 = 0.4
        # conflictSecs1 = \
        #     [sec.Sector(vec.Point(1.43379, -0.564995),
        #                 vec.Point(1.79829, 0.146708), 3)]
        # path1 = pth.Path(oAB1, conflictSecs1, r1)
        # tb1 = turtle.Turtlebot(0, clockwise1, pStart1, ncycle1, path1)
        # tb1.calcSpecialPts()
        # fleet.append(tb1)
        # # ###############################################
        # pStart2 = vec.Point(-0.5991, -0.9)
        # ncycle2 = 5
        # clockwise2 = False
        # oAB2 = vec.Point(-0.6, -0.7)
        # r2 = 0.2
        # conflictSecs2 = \
        #     [sec.Sector(vec.Point(-0.682213, -0.516938),
        #                 vec.Point(-0.41345, -0.762832), 4)]
        # path2 = pth.Path(oAB2, conflictSecs2, r2)
        # tb2 = turtle.Turtlebot(1, clockwise2, pStart2, ncycle2, path2)
        # tb2.calcSpecialPts()
        # fleet.append(tb2)
        # ###############################################
        # pStart3 = vec.Point(0.3, 2.09)
        # ncycle3 = 6
        # clockwise3 = False
        # oAB3 = vec.Point(0.3, 1.7)
        # r3 = 0.4
        # conflictSecs3 = \
        #     [sec.Sector(vec.Point(0.698148, 1.78483),
        #                 vec.Point(-0.0206425, 1.93184), 2)]
        # path3 = pth.Path(oAB3, conflictSecs3, r3)
        # tb3 = turtle.Turtlebot(2, clockwise3, pStart3, ncycle3, path3)
        # tb3.calcSpecialPts()
        # fleet.append(tb3)
        # ###############################################
        # pStart4 = vec.Point(0.4, -0.6)
        # ncycle4 = 4
        # clockwise4 = False
        # oAB4 = vec.Point(0.4, 0.4)
        # r4 = 1
        # conflictSecs4 = \
        #     [sec.Sector(vec.Point(1.37961, 0.593953),
        #                 vec.Point(0.836053, -0.50194), 3),
        #      sec.Sector(vec.Point(-0.287827, 1.13303),
        #                 vec.Point(0.956476, 1.2303), 2),
        #      sec.Sector(vec.Point(-0.510691, 0.00681558),
        #                 vec.Point(-0.500297, 0.824676), 1),
        #      sec.Sector(vec.Point(-0.0307727, -0.505017),
        #                 vec.Point(-0.455843, -0.118798), 4)]
        # path4 = pth.Path(oAB4, conflictSecs4, r4)
        # tb4 = turtle.Turtlebot(3, clockwise4, pStart4, ncycle4, path4)
        # tb4.calcSpecialPts()
        # fleet.append(tb4)
        # ###############################################
        # pStart5 = vec.Point(1.7, 1.3)
        # ncycle5 = 10
        # clockwise5 = False
        # oAB5 = vec.Point(1.7, 1.7)
        # r5 = 0.4
        # conflictSecs5 = \
        #     [sec.Sector(vec.Point(1.30986, 1.60188),
        #                 vec.Point(1.45017, 2.01243), 5)]
        # path5 = pth.Path(oAB5, conflictSecs5, r5)
        # tb5 = turtle.Turtlebot(4, clockwise5, pStart5, ncycle5, path5)
        # tb5.calcSpecialPts()
        # fleet.append(tb5)
        # ###############################################
        # pStart6 = vec.Point(-1.01, 1.0)
        # clockwise6 = False
        # ncycle6 = 4
        # oAB6 = vec.Point(-0.4, 1)
        # r6 = 0.61
        # conflictSecs6 = \
        #     [sec.Sector(vec.Point(-0.174702, 0.434773),
        #                 vec.Point(-0.958526, 0.757681), 1),
        #      sec.Sector(vec.Point(-0.517895, 1.59847),
        #                 vec.Point(0.198975, 0.89811), 2)]
        # path6 = pth.Path(oAB6, conflictSecs6, r6)
        # tb6 = turtle.Turtlebot(5, clockwise6, pStart6, ncycle6, path6)
        # tb6.calcSpecialPts()
        # fleet.append(tb6)
        # ################################################

        ###############################################
        # Standard 2 robots and a tunnel
        # ################################################
        pStart1 = vec.Point(1.00122, 0.49946)
        clockwise1 = False
        ncycle1 = 5
        oAB1 = vec.Point(0.5, 0.5)
        r1 = 0.5
        conflictSecs1 = \
            [sec.Sector(vec.Point(0.074658, 0.238674),
                        vec.Point(0.0699656, 0.75484), 1)]
        path1 = pth.Path(oAB1, conflictSecs1, r1)
        tb1 = turtle.Turtlebot(0, clockwise1, pStart1, ncycle1, path1)
        tb1.calcSpecialPts()
        fleet.append(tb1)
        # ###############################################
        pStart2 = vec.Point(-1.29912, 0.49903)
        ncycle2 = 5
        clockwise2 = True
        oAB2 = vec.Point(-0.8, 0.5)
        r2 = 0.5
        conflictSecs2 = \
            [sec.Sector(vec.Point(-0.36643, 0.750148),
                        vec.Point(-0.371122, 0.238674), 1)]
        path2 = pth.Path(oAB2, conflictSecs2, r2)
        tb2 = turtle.Turtlebot(1, clockwise2, pStart2, ncycle2, path2)
        tb2.calcSpecialPts()
        fleet.append(tb2)
        # ###############################################

        # ################################################
        # 4 robots in 07/C-3
        # ################################################
        # pStart1 = vec.Point(2.00122, -0.20054)
        # clockwise1 = False
        # ncycle1 = 5
        # oAB1 = vec.Point(1.6, -0.2)
        # r1 = 0.4
        # conflictSecs1 = \
        #     [sec.Sector(vec.Point(1.43379, -0.564995),
        #                 vec.Point(1.79829, 0.146708), 3)]
        # path1 = pth.Path(oAB1, conflictSecs1, r1)
        # tb1 = turtle.Turtlebot(0, clockwise1, pStart1, ncycle1, path1)
        # tb1.calcSpecialPts()
        # fleet.append(tb1)
        # # ###############################################
        # pStart2 = vec.Point(-0.5991, -0.9)
        # ncycle2 = 5
        # clockwise2 = False
        # oAB2 = vec.Point(-0.6, -0.7)
        # r2 = 0.2
        # conflictSecs2 = \
        #     [sec.Sector(vec.Point(-0.682213, -0.516938),
        #                 vec.Point(-0.41345, -0.762832), 2)]
        # path2 = pth.Path(oAB2, conflictSecs2, r2)
        # tb2 = turtle.Turtlebot(1, clockwise2, pStart2, ncycle2, path2)
        # tb2.calcSpecialPts()
        # fleet.append(tb2)
        # ###############################################
        # pStart3 = vec.Point(0.3, 2.09)
        # ncycle3 = 6
        # clockwise3 = False
        # oAB3 = vec.Point(0.3, 1.7)
        # r3 = 0.4
        # conflictSecs3 = \
        #     [sec.Sector(vec.Point(0.698148, 1.78483),
        #                 vec.Point(-0.0206425, 1.93184), 1)]
        # path3 = pth.Path(oAB3, conflictSecs3, r3)
        # tb3 = turtle.Turtlebot(2, clockwise3, pStart3, ncycle3, path3)
        # tb3.calcSpecialPts()
        # fleet.append(tb3)
        # ###############################################
        # pStart4 = vec.Point(0.4, -0.6)
        # ncycle4 = 4
        # clockwise4 = False
        # oAB4 = vec.Point(0.4, 0.4)
        # r4 = 1
        # conflictSecs4 = \
        #     [sec.Sector(vec.Point(1.37961, 0.593953),
        #                 vec.Point(0.836053, -0.50194), 3),
        #      sec.Sector(vec.Point(-0.287827, 1.13303),
        #                 vec.Point(0.956476, 1.2303), 1),
        #      sec.Sector(vec.Point(-0.0307727, -0.505017),
        #                 vec.Point(-0.455843, -0.118798), 2)]
        # path4 = pth.Path(oAB4, conflictSecs4, r4)
        # tb4 = turtle.Turtlebot(3, clockwise4, pStart4, ncycle4, path4)
        # tb4.calcSpecialPts()
        # fleet.append(tb4)
        ###############################################


        #############################################
        # Show stages of robots with requested resources for creating resourceList
        stages = []
        for robot in fleet:
            stages.append(robot.getStages())
            print(robot.getStages())
        resourceList = resourceList.ResourceList(stages)
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
