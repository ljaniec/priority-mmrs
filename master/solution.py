# !/usr/bin/python
#-*- encoding: utf-8 -*-
import automat
import copy
import math
import resourceList

# Power set of solutions - 2^n solutions
# iterating over all possible outcomes
# then checking, if they are correct
def generateAllSolutions(requestStructure, resourceList):
    solutions = []
    for i in range(2**(len(requestStructure))):
        k = i
        s = Solution(requestStructure)
        for j in range(len(requestStructure)):
            if k%2:
                s.reqStruct[j].isAccepted = True
            k = k >> 1
        #print('Ocena rozwiazan')
        #print(s)
        if s.isCorrect(resourceList):
            solutions.append(s)
            #print('Rozwiazanie poprawne')
        #else:
            #print('Rozwiazanie niepoprawne')
    if not solutions:
        print('Error! Lack of solutions!')
    return solutions


# Class of solution for requests structure
class Solution(object):
    def __init__(self, requestStructure):
        self.value = -1.0
        self.reqStruct = copy.deepcopy(requestStructure)

    def __eq__(self, solution2):
        if isinstance(solution2, Solution):
            for i in range(len(self.reqStruct)):
                if self.reqStruct[i].isAccepted != solution2.reqStruct[i].isAccepted :
                    return False
            return True
        raise ValueError('Not a solution')

    def __str__(self): # For printing
        return "Value: " + str(self.value) + "  " + str(self.reqStruct)

    def __repr__(self): # For printing
        return "Value: " + str(self.value) + "  " + str(self.reqStruct)

    # Checking, if solution is correct, that's it, there isn't any pair of requests
    # which are both accepted and both need the same resource (even one) 
    # nie istnieja dwa zaakceptowane requesty wymagajace tego samego zasobu
    # TODO: brak badania czy zasob jest zajety wczesniej
    def isCorrect(self, resourceList):
        accepted = []
        for req in self.reqStruct:
            if req.isAccepted:
                accepted.append(copy.deepcopy(req))
        for r1 in accepted: # ?
            if len(set(r1.resources) & set(resourceList)):
                return False
        for r1 in accepted:
            for r2 in accepted:
                if r1.robotID != r2.robotID:
                    if len(set(r1.resources) & set(r2.resources)): 
                        return False
        return True


if __name__ == "__main__":
    requests1 = [automat.Request(1, [1, 2]), automat.Request(2, [3, 4])]
    sol1 = Solution(requests1)
    sol2 = Solution(requests1)
    print('Wynik: ', sol1 == sol2)
    sol1.reqStruct[0].isAccepted = True
    print('Wynik: ', sol1 == sol2)
    # print('Is sol1 correct? ', sol1.isCorrect())
    # print('Is sol2 correct? ', sol2.isCorrect())
    requests2 = [automat.Request(1, [1, 2]), automat.Request(2, [1, 3, 4])]
    sol3 = Solution(requests2)
    sol3.reqStruct[0].isAccepted = True
    sol4 = Solution(requests2)
    sol4.reqStruct[0].isAccepted = True
    sol4.reqStruct[1].isAccepted = True
    # print('Is sol3 correct? ', sol3.isCorrect())
    # print('Is sol4 correct? ', sol4.isCorrect())
