#!/usr/bin/python

import unittest

import sector as sec
import vector as vec
import math


class Test_angleVectors(unittest.TestCase):
    # Angle between vectors (going counterclockwise from pA to pB)
    def test_angleVectors1(self):
        pA = vec.Point(1.0, 1.0)
        pB = vec.Point(2.0, 0.0)
        oAB = vec.Point(1.0, 0.0)
        self.assertEqual(sec.angleVectors(pB, pA, oAB), math.pi/2.0)

    def test_angleVectors2(self):
        pA = vec.Point(1.0, 1.0)
        pB = vec.Point(2.0, 0.0)
        oAB = vec.Point(1.0, 0.0)
        self.assertEqual(sec.angleVectors(pA, pB, oAB), 3.0*math.pi/2.0)
