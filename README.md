# priority-mmrs

Implementation of hybrid (event and continuous time based) Multiple Mobile Robot System with priority functions.

Do każdej z symulacji potrzebne są wyznaczone ręcznie / z użyciem programu CAD współrzędne punktów (dla każdego z robotów):
* punkt startowy `pStart`
* określenie, czy ruch jest zgodny z ruchem wskazówek zegara `clockwise`
* liczba okrążeń `nCycle`
* środek okręgu-ścieżki `oAB`
* promień ww. `r`
* lista początku i końca sektora konfliktowego z przypisanym zasobem, wzór:
```
conflictSecs =[sec.Sector(vec.Point(1.4115965384, 4.2153083737), 
                        vec.Point(2.7678179328, 4.9864771551), 1),
               sec.Sector(vec.Point(4.9864771551, 2.7678179328), 
                        vec.Point(4.2153083737, 1.4115965384), 2)]
```
UWAGA, początek/koniec sektora definiowany nie z perspektywy robota (wjazd przez początek, opuszczenie przez koniec), a zawsze zgodnie z ruchem wskazówek zegara globalnie.


Z tego definiujemy ścieżkę `path = Path(oAB, conflictSecs, r)`, robota `tb = Turtlebot(indeks, clockwise, pStart, ncycle, path)`, z metody `calcSpecialPts()` robota tb wyznaczamy pozostałe punkty specjalne i na końcu dodajemy robota do floty `fleet`. Indeks robota jest widoczny w ROS jako pomniejszony o jeden.

Do wpisanych tutaj danych musi powstać odpowiadający mu plik `workspace.world` + trzeba zmieniać ewentualnie liczbę robotów `amountOfRobots` w `config.py`.
Zmieniać w `workspace.world` trzeba kolejne TB2, tj.
```
turtlebot
(
  name "robot_0"
  color "red"
  # pose of the model in the world [x y z heading(degrees)]
  pose [1 3 0.000 270.000]
)
```
Dokładniej, ich nazwę (tu stosowany styl `robot_N`, `N` e {0,...9}, ważne, bo tak wątki w ROS od Stage są potem), kolor i pozycję (x, y, z, theta).
Obrazek pod tło robiony był ręcznie z użyciem proporcji pod odpowiedni wymiar.

Komendy w terminalu (zakładam `.bashrc` z `source /opt/ros/noetic/setup.bash`):
1) `roscore` w terminalu A
2) `rosrun stage_ros stageros workspace_n6.world` w terminalu B
3) (po uruchomieniu Stage) `python3 control.py` w terminalu C, w folderze z skryptami

Roboty są "gadatliwe", podobnie sterownik. Do wizualizacji służy terminal z oczyszczonymi komunikatami.

# ROS i jego wątki w użyciu z symulatorem Stage (pod 2-6 robotów)

Stage automatycznie tworzy wątki dla robotów postaci `/robot_0/...`, jak `/base_pose_ground_truth`, `/cmd_vel` i `/odom` (z kolejnymi liczbami za zero), stąd specyficzne nazwy reszty wątków z `robot_ID` , aby utrzymać konwencję. 

Pozostałe opisane niżej wątki służą komunikacji zdarzeniowej (special point i żądanie zasobu z GEPP)

Ich przykład dla 2 robotów:
```
/GEPP_request # żądanie zasobów
/clock # zegar Stage
/new_stage_info # wejście w nowy (kolejny) punkt specjalny (Approach Point/Critical Point/Release Point)
/robot_0/base_pose_ground_truth # nieużywane
/robot_0/cmd_vel # do sterowania prędkościami robota - liniową i kątową
/robot_0/odom # z tego (x, y)
/robot_1/base_pose_ground_truth # nieużywane
/robot_1/cmd_vel # jw.
/robot_1/odom # jw.
/rosout # std
/rosout_agg # std
/tf # std
```

Do komunikacji z ROS-em w 07 - w terminalu trzeba wpisać:
```
export ROS_MASTER_URI="http://192.168.5.50:11311" # to stałe 
export ROS_IP="192.168.5.103" # to IP trzeba własne
```
